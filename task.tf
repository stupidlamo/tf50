variable "node_count" {
  default = "3"
}

resource "google_compute_instance" "default" {
  count = "${var.node_count}"
  name  = "my-${count.index + 1}-node"
  machine_type = "e2-small"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
      size = 20
    }
  }
  network_interface {
  network = "default"
  }
}
